const isValidExtFile = (str) => /^\S+\.(log|txt|json|yaml|xml|js)$/gi.test(str);

module.exports = {
  isValidExtFile
}
