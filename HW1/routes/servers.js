const { Router } = require('express');
const { getAll, create, getFilename, deleteFile, editFile } = require ('../controllers/servers.js');
const router = Router();

router.get('/api/files', getAll);

router.post('/api/files', create);

router.get('/api/files/:filename', getFilename);

router.delete('/api/files/:filename', deleteFile);

router.post('/api/files/edit', editFile);

module.exports = router;
